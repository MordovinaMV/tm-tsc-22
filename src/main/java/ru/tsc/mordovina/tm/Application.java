package ru.tsc.mordovina.tm;

import ru.tsc.mordovina.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}