package ru.tsc.mordovina.tm.api.entity;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

    void setCreated(Date created);

}