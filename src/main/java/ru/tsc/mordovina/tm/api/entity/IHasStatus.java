package ru.tsc.mordovina.tm.api.entity;

import ru.tsc.mordovina.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}