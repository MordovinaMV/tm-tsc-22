package ru.tsc.mordovina.tm.api.repository;

public interface IAuthRepository {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId);

}
