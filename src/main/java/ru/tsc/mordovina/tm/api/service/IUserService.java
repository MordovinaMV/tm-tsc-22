package ru.tsc.mordovina.tm.api.service;

import ru.tsc.mordovina.tm.api.IService;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String id, String password);

    User setRole(String userId, Role role);

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User removeUserById(String id);

    User removeUserByLogin(String login);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User updateUserById(String id, String lastName, String firstName, String middleName, String email);

    User updateUserByLogin(String login, String lastName, String firstName, String middleName, String email);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

}
