package ru.tsc.mordovina.tm.command;

import ru.tsc.mordovina.tm.exception.empty.EmptyNameException;
import ru.tsc.mordovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.mordovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.Task;

import java.util.List;
import java.util.Optional;

public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    protected void showProjectTasks(final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
        final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(
                project.getUserId(), project.getId()
        );
        if (tasks.size() <= 0) throw new TaskNotFoundException();
        for (Task task : tasks)
            showTask(task);
    }

    protected void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    protected void showTask(final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

    protected Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description);
    }

}
