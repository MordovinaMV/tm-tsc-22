package ru.tsc.mordovina.tm.command.system;

import ru.tsc.mordovina.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return "version";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Display program version";
    }

    @Override
    public void execute() {
        System.out.println("1.10.0");
    }

}
