package ru.tsc.mordovina.tm.command.task;

import ru.tsc.mordovina.tm.command.AbstractTaskCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public String getCommand() {
        return "task-create";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Create task";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
    }

}
