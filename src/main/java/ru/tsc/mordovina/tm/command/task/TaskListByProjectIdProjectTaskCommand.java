package ru.tsc.mordovina.tm.command.task;

import ru.tsc.mordovina.tm.command.AbstractProjectTaskCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class TaskListByProjectIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public String getCommand() {
        return "task-list-by-project-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display list of tasks by project id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        showProjectTasks(project);
    }

}
