package ru.tsc.mordovina.tm.command.task;

import ru.tsc.mordovina.tm.command.AbstractTaskCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.enumerated.Sort;
import ru.tsc.mordovina.tm.model.Task;
import ru.tsc.mordovina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public final class TaskListShowCommand extends AbstractTaskCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public String getCommand() {
        return "task-list";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final List<Task> tasks;
        if (!Optional.ofNullable(sort).isPresent())
            tasks = serviceLocator.getTaskService().findAll(userId);
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(userId, sortType.getComparator());
        }
        for (Task task : tasks) showTask(task);
    }

}