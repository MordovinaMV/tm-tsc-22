package ru.tsc.mordovina.tm.command.user;

import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.exception.system.AccessDeniedException;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public final class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getCommand() {
        return "user-remove-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove user by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId))
            throw new AccessDeniedException();
        serviceLocator.getUserService().removeUserById(id);
    }

}
