package ru.tsc.mordovina.tm.exception.empty;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error. Status is empty");
    }

}
