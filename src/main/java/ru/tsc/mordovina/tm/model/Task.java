package ru.tsc.mordovina.tm.model;

import ru.tsc.mordovina.tm.api.entity.IWBS;
import ru.tsc.mordovina.tm.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractOwnerEntity implements IWBS {

    public Task() {
    }

    public Task(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private String projectId = null;

    private Date startDate;

    private Date finishDate;

    private Date created = new Date();

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

}
