package ru.tsc.mordovina.tm.repository;

import ru.tsc.mordovina.tm.api.repository.IOwnerRepository;
import ru.tsc.mordovina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @Override
    public E add(String userId, E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());

    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());

    }

    @Override
    public E findById(final String userId, final String id) {
        return findAll(userId).stream()
                .filter(e -> e.getId().equals(id))
                .findFirst().orElse(null);

    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public void clear(final String userId) {
        List<String> entityUserId = list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .map(E::getId)
                .collect(Collectors.toList());
        entityUserId.forEach(list::remove);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public void remove(final String userId, final E entity) {
        list.remove(entity.getId());
    }

    public Integer getSize(final String userId) {
        return list.size();
    }

}
