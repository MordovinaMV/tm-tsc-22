package ru.tsc.mordovina.tm.repository;

import ru.tsc.mordovina.tm.api.repository.IAuthRepository;

public final class AuthRepository implements IAuthRepository {

    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(final String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
