package ru.tsc.mordovina.tm.repository;

import ru.tsc.mordovina.tm.api.repository.IUserRepository;
import ru.tsc.mordovina.tm.model.User;

import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public static Predicate<User> predicateByLogin(final String login) {
        return u -> login.equals(u.getLogin());
    }

    public static Predicate<User> predicateByEmail(final String email) {
        return u -> email.equals(u.getEmail());
    }

    @Override
    public User findUserByLogin(final String login) {
        return list.stream()
                .filter(u -> u.getLogin().toLowerCase().equals(login.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findUserByEmail(final String email) {
        return list.stream()
                .filter(predicateByEmail(email))
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeUserById(final String id) {
        final User user = findById(id);
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findUserByLogin(login);
        list.remove(user);
        return user;
    }

    @Override
    public boolean userExistsByLogin(final String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public boolean userExistsByEmail(final String email) {
        return findUserByEmail(email) != null;
    }

}
